# kopioi edellisestä tehtävästä funktion viiva koodi tänne, ja toteuta funktio kuvio sitä hyödyntäen
# funktiota kannattaa testata kutsumalla sitä täällä seuraavasti
def viiva(kertaa, asia):
    print(asia[0:1]*kertaa)
    if asia == "":
        print("*"*kertaa)
def kolmio(koko, asia):
    num0 = 0
    num1 = 1
    while True:
        if koko == num0:
            break
        num0 += 1
        viiva(num1, asia)
        num1 += 1
def nelio(koko, merkki, pitka):
    num0 = 0
    while True:
        if pitka == num0:
            break
        num0 += 1
        viiva(koko, merkki)
def kuvio(a, b, c, d):
    kolmio(a, b)
    nelio(a, d, c)
if __name__ == "__main__":
    kuvio(5, "x", 2, "o")
