# Tämä ohjelma kysyy käyttäjältä, että haluaako hän lisää tai poistaa numeroita, tai lopettaa ohejelman
# Tulostus pitäisi näyttää tältä
# (l)
# Lista on nyt [1]
# (l)
# Lista on nyt [1, 2]
# (l)
# Lista on nyt [1, 2 , 3]
# (p)
# Lista on nyt [1, 2]
# (x)
# Moi!
lista = []
print("Lista on nyt " + str(lista))
while True:
    num1 = 1
    kysy = input("(l)isää, (p)oista vai e(x)it: ")
    if kysy == "l":
        lista.append(num1)
        num1 += 1
        print("Lista on nyt " + str(lista))
    if kysy == "p":
        lista.remove(len(lista))
        print("Lista on nyt " + str(lista))
        num1 -= 1
    if kysy == "x":
        print("Moi!")
        break
