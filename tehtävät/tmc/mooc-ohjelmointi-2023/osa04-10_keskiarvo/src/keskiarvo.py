def keskiarvo(lista0):
    num0 = 0
    num1 = 0
    num2 = len(lista0)
    num3 = 0
    while True:
        if num2 == num0:
            return(num3 / num2)
        num1 = lista0[num0]
        num3 += num1
        num0 += 1
    
if __name__ == "__main__":
    lista = [3, 6, -4] 
    tulos = keskiarvo(lista) 
    print(tulos)
