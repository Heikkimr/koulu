#  Tee ohjelma, joka kysyy käyttäjältä “Monesko nimi tulostetaan?”. Sitten ohjelma tulostaa listasta sen sukunimen, joka on järjestyksessä se käyttäjän antama järjestysluku.
lista0 = ["Vanni", "Visanti", "Rantasalo", "Wuorimaa", "Kilpi", "Jalas", "Kaira", "Poijärvi", "Linnala", "Koskenniemi", "Arni", "Hainari",
"Pohjanpalo", "Jännes", "Kuusi", "Talas", "Rautapää", "Aura", "Wiherheimo", "Kuusisto", "Rantakari", "Pinomaa", "Paasilinna", "Pihkala",
"Halsti", "Kallia", "Haarla", "Harva", "Heikinheimo", "Päivänsalo", "Helanen", "Hattara", "Helismaa"]
check = input("Mitä kirjainta etsitään sukunimen 3:sta merkistä? ")
num1 = int(input("Monesko merkki sukunimessä? "))
lista1 = [idx for idx in lista0 if idx[num1].lower() == check.lower()]
num0 = int(input("Monesko nimi tulostetaan?"))
while True:
    if num0 == len(lista1):
        break
    print(lista1[num0])
    break
