# kopioi edellisestä tehtävästä funktion viiva koodi tänne
def viiva(kertaa, asia):
    print(asia[0:1]*kertaa)
    if asia == "":
        print("*"*kertaa)
def nelio(koko, merkki):
    num0 = 0
    while True:
        if koko == num0:
            break
        num0 += 1
        viiva(koko, merkki)
# funktiota kannattaa testata kutsumalla sitä täällä seuraavasti
if __name__ == "__main__":
    nelio(5, "x")
