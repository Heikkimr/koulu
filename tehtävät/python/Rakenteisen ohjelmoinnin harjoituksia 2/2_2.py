# 2) Tee ohjelma, joka kysyy käyttäjältä “Millä alkukirjaimella etsitään?”. Ohjelma tulostaa allekkain listasta ne sukunimet, jotka alkavat käyttäjän antamalla kirjaimella.
lista0 = ["Vanni", "Visanti", "Rantasalo", "Wuorimaa", "Kilpi", "Jalas", "Kaira", "Poijärvi", "Linnala", "Koskenniemi", "Arni", "Hainari",
"Pohjanpalo", "Jännes", "Kuusi", "Talas", "Rautapää", "Aura", "Wiherheimo", "Kuusisto", "Rantakari", "Pinomaa", "Paasilinna", "Pihkala",
"Halsti", "Kallia", "Haarla", "Harva", "Heikinheimo", "Päivänsalo", "Helanen", "Hattara", "Helismaa"]
check = input("Millä kirjaimella etsitään?")
res = [idx for idx in lista0 if idx[0].lower() == check.lower()]
print(res)
