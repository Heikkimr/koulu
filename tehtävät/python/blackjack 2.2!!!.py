import random
import os 
from os import system, name
from time import sleep
def clear():
    if name == 'nt':
        _ = system('cls')
    else:
        _ = system('clear')
carda = " _____  |A .  || /.\ ||(_._)||  |  ||____A|"
card2 = " _____  |2    ||  ^  ||     ||  ^  ||___ 2|"
card3 = " _____  |3    || ^ ^ ||     ||  ^  ||___ 3|"
card4 = " _____  |4    || ^ ^ ||     || ^ ^ ||____4|"
card5 = " _____  |5    || ^ ^ ||  ^  || ^ ^ ||____5|"
card6 = " _____  |6    || ^ ^ || ^ ^ || ^ ^ ||____6|"
card7 = " _____  |7    || ^ ^ ||^ ^ ^|| ^ ^ ||____7|"
card8 = " _____  |8    ||^ ^ ^|| ^ ^ ||^ ^ ^||____8|"
card9 = " _____  |9    ||^ ^ ^||^ ^ ^||^ ^ ^||____9|"
card10 = " _____  |10^  ||^ ^ ^||^ ^ ^||^ ^ ^||___10|"
cardj = " _____  |J  ww|| ^ {)||(.)%%|| | % ||__%%J|"
cardq = " _____  |Q  ww|| ^ {(||(.)%%|| |%%%||_%%%Q|"
cardk = " _____  |K  WW|| ^ {)||(.)%%|| |%%%||_%%%K|"
cardque = " _____  |?    ||     ||     ||     ||____?|"
def drawcardP():
    num0 = random.randint(1, 13)
    global player
    global playerI
    if num0 > 10:
        if num0 == 11:
            player += "J"
        elif num0 == 12:
            player += "Q"
        elif num0 == 13:
            player += "K"
    elif num0 == 1:
        player += "A"
    elif num0 == 10:
        player += "T"
    else:
        player.append(num0)   
    if num0 >= 11 and num0 <= 13:
        playerI.append(10)
    elif num0 == 1:
        playerI.append(11)
    else:
        playerI.append(num0)
def drawcardD():
    num0 = random.randint(1, 13)
    global dealer
    global dealerI
    if num0 >= 10:
        if num0 == 10:
            dealer += "T"
        elif num0 == 11:
            dealer += "J"
        elif num0 == 12:
            dealer += "Q"
        elif num0 == 13:
            dealer += "K"
    elif num0 == 1:
        dealer += "A"
    else:
        dealer.append(num0)   
    if num0 >= 11 and num0 <= 13:
        dealerI.append(10)
    elif num0 == 1:
        dealerI.append(11)
    else:
        dealerI.append(num0)
def bust(target):
    if value(target) > 21:
        return(True)
    else:
        return(False)
def value(target):
    num0 = 0
    num1 = 0
    ace = 0
    while num0 < len(target):
        if target[num0] == 11:
            ace += 1
        num0 += 1
    for number in target:
        num1 += number
    if num1 > 21 and ace > 0:
        num1 -= 10
        ace -= 1
    return(int(num1))
def blackjack(target):
    if len(target) == 2:
        if target[0] == 11 and target[1] == 10:
            return(True)
        elif target[1] == 11 and target[0] == 10:
            return(True)
def blackjackplayer():
    if blackjack(playerI) == True:
        if dealer[(len(dealer) - 1)] == "?":
            dealer.remove("?")
        while value(dealerI) < 17:
            drawcardD()
            printgame()
            sleep(1)
        if value(dealerI) == 21:
            return(False)
        else:
            return(True)
def win():
    if blackjackplayer() == True:
        return("bjp")
    elif blackjackplayer() == False:
        return("Draw")
    elif blackjack(dealerI) == True and value(playerI) != 21:
        return("bjd")
    elif bust(dealerI) == True:
        return("BustD")
    elif value(playerI) and bust(playerI) == False and value(dealerI) < value(playerI) and value(dealerI) > 16: 
        return(True)
    elif bust(playerI) == True:
        return("BustP")
    elif value(playerI) < value(dealerI) and stand == True:
        return(False)
    elif value(playerI) == value(dealerI) and stand == True:
        return("Draw")
    else:
        return("no")
def returncard(inputcard):
    global print1,print2,print3,print3,print4,print5,print6,carda,card2,card3,card4,card5,card6,card7,card8,card9,card10,cardj,cardj,cardq,cardk
    print1 += inputcard[0:7]
    print2 += inputcard[8:15]
    print3 += inputcard[15:22]
    print4 += inputcard[22:29]
    print5 += inputcard[29:36]
    print6 += inputcard[36:43]
def printcards(input): 
    global print1,print2,print3,print3,print4,print5,print6,carda,card2,card3,card4,card5,card6,card7,card8,card9,card10,cardj,cardj,cardq,cardk
    num0 = 0
    print1 = ""
    print2 = ""
    print3 = ""
    print4 = ""
    print5 = ""
    print6 = ""
    while num0 < len(input):
        if input[num0] == "A":
            returncard(carda)
        if input[num0] == "2":
            returncard(card2)
        if input[num0] == "3":
            returncard(card3)
        if input[num0] == "4":
            returncard(card4)
        if input[num0] == "5":
            returncard(card5)
        if input[num0] == "6":
            returncard(card6)
        if input[num0] == "7":
            returncard(card7)
        if input[num0] == "8":
            returncard(card8)
        if input[num0] == "9":
            returncard(card9)
        if input[num0] == "T":
            returncard(card10)
        if input[num0] == "J":
            returncard(cardj)
        if input[num0] == "Q":
            returncard(cardq)
        if input[num0] == "K":
            returncard(cardk)
        if input[num0] == "?":
            returncard(cardque)
        num0 += 1
    print(f"{print1}\n{print2}\n{print3}\n{print4}\n{print5}\n{print6}")
def printgame():
    temp = ""
    num0 = 0
    sleep(1)
    clear()
    print(f"-----------------------\nDealer {value(dealerI)}")
    while num0 < len(dealer):
        temp += str(dealer[num0])
        num0 += 1
    printcards(temp)
    print(f"-----------------------\nPlayer {value(playerI)}")
    temp = ""
    num0 = 0
    while num0 < len(player):
        temp += str(player[num0])
        num0 += 1
    printcards(temp)
    print("-----------------------") 
def printresult(result):
    clear()
    if result == True:
        print("-----------------------\nYou win with " + str(value(playerI)) + " points!\n-----------------------")
    elif result == "bjp":
        print("-----------------------\nYou won with blackjack!\n-----------------------")
    elif result == "bjd":
        print("-----------------------\nDealer won with blackjack!\n-----------------------")
    elif result == False:
        print("-----------------------\nDealer won with " + str(value(dealerI)) +" points!\n-----------------------")
    elif result == "Draw":
        print("-----------------------\nYou Drew!\n-----------------------")
    elif result == "BustP":
        print("-----------------------\nPlayer went bust!\n-----------------------")
    elif result == "BustD":
        print("-----------------------\nDealer went bust!\n-----------------------")
    else:
        print("The game has encountered an error.")
while True:
    player = []
    dealer = []
    playerI = []
    dealerI = []
    stand = False
    drawcardD()
    dealer += "?"
    drawcardP()
    drawcardP()
    printgame()
    if win() != "no":
        printresult(win())
    else:
        while True:
            hos = input("(S)tand or (H)it: ")
            hos = hos.lower()
            if hos == "h":
                drawcardP()
                printgame()
            if hos == "s":
                stand = True
                break
            if win() != "no":
                sleep(1)
                printresult(win())
                break
    if win() == "no":
        dealer.remove("?")
        while value(dealerI) < 17:
            drawcardD()
            printgame()
        printgame()
        if win() != "no":
            printresult(win())