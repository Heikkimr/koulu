def poista_isot(lista):
    num0 = 0
    temp = ""
    lista_uus = []
    while num0 < len(lista):
        temp = lista[num0]
        if temp.isupper() == False:
            lista_uus.append(temp)
            num0 += 1
        else:
            num0 += 1
    return(lista_uus)
if __name__ == "__main__":
    lista = ["ABC", "def", "ISO", "TOINENISO", "pieni", "toinen pieni", "Osittain Iso"]
    karsittu_lista = poista_isot(lista)
    print(karsittu_lista)