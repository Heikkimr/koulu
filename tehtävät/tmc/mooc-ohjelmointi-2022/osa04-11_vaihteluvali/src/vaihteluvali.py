def vaihteluvali(lista0):
    return (sorted(lista0)[len(lista0) - 1] - sorted(lista0)[0])
if __name__ == "__main__":
    lista = [3, 6, -4] 
    tulos = vaihteluvali(lista) 
    print(tulos)