import random
# Miksi in player ja playerI?
# playerI on vain siihen käyttöön että se voi tarkistaa pisteet helpommin, ennen se koodi oli niin buginen että peli ei toiminut kunolla
# antaa pelaajalle kortin
def drawcardP():
    num0 = random.randint(1, 13)
    global player
    global playerI
    if num0 > 10:
        if num0 == 11:
            player += "J"
        elif num0 == 12:
            player += "Q"
        elif num0 == 13:
            player += "K"
    elif num0 == 1:
        player += "A"
    else:
        player.append(num0)   
    if num0 >= 11 and num0 <= 13:
        playerI.append(10)
    elif num0 == 1:
        playerI.append(11)
    else:
        playerI.append(num0)
# Antaa diilerille kortin
def drawcardD():
    num0 = random.randint(1, 13)
    global dealer
    global dealerI
    if num0 > 10:
        if num0 == 11:
            dealer += "J"
        elif num0 == 12:
            dealer += "Q"
        elif num0 == 13:
            dealer += "K"
    elif num0 == 1:
        dealer += "A"
    else:
        dealer.append(num0)   
    if num0 >= 11 and num0 <= 13:
        dealerI.append(10)
    elif num0 == 1:
        dealerI.append(11)
    else:
        dealerI.append(num0)
# tarkistaa, että onko joku mennyt yli 21
def bust(target):
    if target == dealer:
        target = dealerI
    if target == player:
        target = playerI
    if value(target) > 21:
        return(True)
    else:
        return(False)
# Palauttaa kohden korttien arvot
def value(target):
    num0 = 0
    num1 = 0
    ace = 0
    while num0 < len(target):
        if target[num0] == "A":
            ace += 1
        num0 += 1
    for number in target:
        num1 += number
    if num1 > 21 and ace > 0:
        num1 -= 10
        ace -= 1
    return(num1)
# Tarkistaa, että onko jollakin blackjacki
def blackjack(target):
    c1 = False
    c2 = False
    if len(target) == 2:
        if target[1] == "A":
            c1 = True
            if target[0] == "K" or "Q" or "J":
                c2 = True
            else:
                c2 = False
                c1 = False
        if target[0] == "A":
            c1 = True
            if target[1] == "K" or "Q" or "J":
                c2 = True
        if c1 and c2 == True:
            return(True)
# Tarkistaa, että onko pelaaja voittanut tai hävinnyt True = pelaaja voitti False = diileri voitti "Draw" = tasapeli "backjackp" = pelaaja voitti blackjackillä "blackjackd" = diileri voitti blackjackillä BustP = pelaaja meni yli 21 BustD = diileri meni yli 21 no = kaikki ei ole tapahtunut
def win():
    if blackjack(player) == True:
        return("Blackjackp")
    elif blackjack(dealer) == True and value(playerI) != 21:
        return("Blackjackd")
    elif bust(dealer) == True:
        return("BustD")
    elif value(playerI) and bust(player) == False and value(dealerI) < value(playerI) and value(dealerI) > 16: 
        return(True)
    elif bust(player) == True:
        return("BustP")
    elif value(playerI) < value(dealerI) and stand == True:
        return(False)
    elif value(playerI) == value(dealerI) and stand == True:
        return("Draw")
    else:
        return("no")
# Tulostaa pelin
def printgame():
    carda = " _____\n|A .  |\n| /.\ |\n|(_._)|\n|  |  |\n|____A|"
    card2 = " _____\n|2    |\n|  ^  |\n|     |\n|  ^  |\n|___ 2|"
    card3 = " _____\n|3    |\n| ^ ^ |\n|     |\n|  ^  |\n|___ 3|"
    card4 = " _____\n|4    |\n| ^ ^ |\n|     |\n| ^ ^ |\n|____4|"
    card5 = " _____\n|5    |\n| ^ ^ |\n|  ^  |\n| ^ ^ |\n|____5|"
    card6 = " _____\n|6    |\n| ^ ^ |\n| ^ ^ |\n| ^ ^ |\n|____6|"
    card7 = " _____\n|7    |\n| ^ ^ |\n|^ ^ ^|\n| ^ ^ |\n|____7|"
    card8 = " _____\n|8    |\n|^ ^ ^|\n| ^ ^ |\n|^ ^ ^|\n|____8|"
    card9 = " _____\n|9    |\n|^ ^ ^|\n|^ ^ ^|\n|^ ^ ^|\n|____9|"
    card10 = " _____\n|10^  |\n|^ ^ ^|\n|^ ^ ^|\n|^ ^ ^|\n|___10|"
    cardj = " _____\n|J  ww|\n| ^ {)|\n|(.)%%|\n| | % |\n|__%%J|"
    cardq = " _____\n|Q  ww|\n| ^ {(|\n|(.)%%|\n| |%%%|\n|_%%%Q|"
    cardk = " _____\n|K  WW|\n| ^ {)|\n|(.)%%|\n| |%%%|\n|_%%%K|"
    print("-----------------------")
    print(f"Dealer {value(dealerI)}")
    num0 = 0
    while num0 < len(dealer):
        if dealer[num0] == "A":
            print(carda)
        if dealer[num0] == 2:
            print(card2)
        if dealer[num0] == 3:
            print(card3)
        if dealer[num0] == 4:
            print(card4)
        if dealer[num0] == 5:
            print(card5)
        if dealer[num0] == 6:
            print(card6)
        if dealer[num0] == 7:
            print(card7)
        if dealer[num0] == 8:
            print(card8)
        if dealer[num0] == 9:
            print(card9)
        if dealer[num0] == 10:
            print(card10)
        if dealer[num0] == "J":
            print(cardj)
        if dealer[num0] == "Q":
            print(cardq)
        if dealer[num0] == "K":
            print(cardk)
        num0 += 1
    print("-----------------------")
    print(f"Player {value(playerI)}")
    num0 = 0
    while num0 < len(player):
        if player[num0] == "A":
            print(carda)
        if player[num0] == 2:
            print(card2)
        if player[num0] == 3:
            print(card3)
        if player[num0] == 4:
            print(card4)
        if player[num0] == 5:
            print(card5)
        if player[num0] == 6:
            print(card6)
        if player[num0] == 7:
            print(card7)
        if player[num0] == 8:
            print(card8)
        if player[num0] == 9:
            print(card9)
        if player[num0] == 10:
            print(card10)
        if player[num0] == "J":
            print(cardj)
        if player[num0] == "Q":
            print(cardq)
        if player[num0] == "K":
            print(cardk)
        num0 += 1
    print("-----------------------")
# printtaa tuloksen  
def printresult(result):
    if result == True:
        print("You win with " + str(value(playerI)) + " points!")
    elif result == False:
        print(f"Dealer won with " + str(value(dealerI)) +" points!")
    elif result == "Draw":
        print("You Drew!")
    elif result == "Blackjackp":
        print("You won with blackjack!")
    elif result == "Blackjackd":
        print("Dealer won with blackjack!")
    elif result == "BustP":
        print("Player went bust!")
    elif result == "BustD":
        print("Dealer went bust!")
    else:
        print("The game has encountered an error.")
# Pelin logiikka
player = []
dealer = []
playerI = []
dealerI = []
stand = False
move = ""
drawcardD()
drawcardP()
drawcardP()
printgame()
if win() == True or win() == False or win() == "Draw" or win() == "Blackjackp" or win() == "Blackjackd" or win() == "BustP" or win() == "BustD":
    printresult(win())
while True:
    hos = input("(S)tand or (H)it: ")
    if hos == "h":
        drawcardP()
        printgame()
    if hos == "s":
        stand = True
        break
    if win() == True or win() == False or win() == "Draw" or win() == "Blackjackp" or win() == "Blackjackd" or win() == "BustP" or win() == "BustD":
        printresult(win())
        break
if win() == "no":
    while value(dealerI) < 17:
        drawcardD()
    printgame()
    if win() == True or win() == False or win() == "Draw" or win() == "Blackjackp" or win() == "Blackjackd" or win() == "BustP" or win() == "BustD":
        printresult(win())