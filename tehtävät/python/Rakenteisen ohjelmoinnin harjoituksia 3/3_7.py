# G) Tee ohjelma, joka kysyy käyttäjältä pistemäärän. Sitten ohjelma tulostaa pistemäärää vastaavan arvosanan. Arvosanan määräytyminen:
# · 0-50 -> 0
# · 51-70 -> 1
# · 71-90 -> 2
# · 91-100 -> 3
koe = int(input("Kerro kokeen pistemäärä: "))
teht = int(input("Kerro suoritetun tehtävien määrä: "))
arvosana = 0
pisteet = 0
if koe > 50 and teht >= 2:
    arvosana = 1
    pisteet = 75
elif koe > 71 and teht >= 5:
    arvosana = 2
    pisteet = 90
elif koe > 90 and teht > 10:
    arvosana = 3
    pisteet = 100
else:
    arvosana = 0
    pisteet = 0
print(arvosana)
