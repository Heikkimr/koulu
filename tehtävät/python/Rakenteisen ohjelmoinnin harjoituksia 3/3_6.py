# F) Tee funktio arvosana, joka laskee opiskelijan lopullisen arvosanan kahdesta arvosta: kokeen pistemäärästä ja suoritettujen harjoitustehtävien lukumäärästä.
# Funktio saa 2 arvoa parametrina: kokeen pistemäärä 0–100 ja suoritetut harjoitustehtävät 0:sta alkaen.
# · 100, jos kokeen pistemäärä on suurempi kuin 90 tai jos suoritettujen harjoitustehtävien määrä en suurempi kuin 10.
# · 90, jos kokeen pistemäärä on suurempi kuin 75 ja suoritettujen harjoitustehtävien määrä on vähintään 5.
# · 75, jos kokeen pistemäärä on suurempi kuin 50 ja suoritettujen harjoitustehtävien määrä on vähintään 2.
# · Muutoin arvosana on
koe = int(input("Kerro kokeen pistemäärä: "))
teht = int(input("Kerro suoritetun tehtävien määrä: "))
arvosana = 0
pisteet = 0
if koe > 90 and teht > 10:
    arvosana = 3
    pisteet = 100
elif koe == 71-90 and teht >= 5:
    arvosana = 2
    pisteet = 90
elif koe == 51-70 and teht >= 2:
    arvosana = 1
    pisteet = 75
else:
    arvosana = 0
    pisteet = 0
print(pisteet)
