def muotoile(lista):
    num0 = 0
    listaMuotoile = []
    while num0 < len(lista):
        listaMuotoile.append(f"{lista[num0]:.2f}")
        num0 += 1
    return(listaMuotoile)
if __name__ == "__main__":
    lista = [1.234, 0.3333, 0.99999, 3.446]
    lista2 = muotoile(lista)
    print(lista2)