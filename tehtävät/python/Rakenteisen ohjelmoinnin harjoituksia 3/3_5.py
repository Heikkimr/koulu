# E) Tee ohjelma, joka kyselee käyttäjältä kokonaislukuja 1 - 9 väliltä ja tallentaa ne listaan. Nolla lopettaa käyttäjän syötteen, eikä sitä tule tallentaa listaan.
# Sitten ohjelma lisää listan alkion arvoon ko. alkion indeksin listassa. Kuitenkin, jos uudeksi arvoksi tulee kaksinumeroinen luku, niin vain viimeinen (ykköset) numero tallennetaan. Sitten ohjelma tulostaa listan.
num0 = 0
num1 = 0
num2 = 0
lista = []
while True:
    num0 = int(input("Kerro numero 1-9: "))
    num2 = num0
    num1 += 1
    num0_str = str(num0 + num1)
    if len(num0_str) == 1:
        num0 = int(num0_str)
    if len(num0_str) > 1:
        num0_str = num0_str[len(num0_str)-1]
        num0 = int(num0_str)
    if num2 == 0:
        print(lista)
        break
    lista.append(num0)
# onko se paras ratkaisu? ei. onko se toimiva? hieman
