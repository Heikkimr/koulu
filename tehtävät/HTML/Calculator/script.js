// Finglish at its best 

let display = document.getElementById('display');
let currentInput = '';
let operator = '';
let previousInput = '';
let decimalClicked = false;

function lisaaNumero(number) {
    if (operaattori != '')
    {
        previousInput = currentInput
    }
    currentInput = ''
    currentInput += number;
    päivitäNäyttö();
}

function lisaaDesimaali() 
{
    if (!decimalClicked) {
        currentInput += '.';
        decimalClicked = true;
        päivitäNäyttö();
    }
}

function operaattori(op) 
{
    operator = op;
    päivitäNäyttö();
}

function puhdistaNäyttö() 
{
  currentInput = '';
  previousInput = '';
  operator = '';
  decimalClicked = false;
  päivitäNäyttö();
}

function laskeTulos() 
{
    let result;
    switch (operator) 
    {
        case '+':
            result = parseFloat(previousInput) + parseFloat(currentInput);
            break;
        case '-':
            result = parseFloat(previousInput) - parseFloat(currentInput);
            break;
        case '*':
            result = parseFloat(previousInput) * parseFloat(currentInput);
            break;
        case '/':
            result = parseFloat(previousInput) / parseFloat(currentInput);
            break;
        default:
            break;
    }
    puhdistaNäyttö();
    currentInput = result.toString();
    päivitäNäyttö();
}

function päivitäNäyttö() 
{
    display.value = currentInput || '0';
}