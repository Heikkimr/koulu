# J) Tee ohjelma, joka kyselee käyttäjältä sanoja ja tallentaa ne listaan. Tyhjä lopettaa. Lopuksi ohjelma tulostaa listan pisimmän, lyhyimmän, ensimmäisen, viimeisen ja keskimmäisen (jos tasajako, niin näistä se eka) sanan.
sana = ""
lista = []
pisin = ""
lyhyin = "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
while True:
    sana = input("Kerro sana: ")
    if sana == "":
        print("Lyhyin on " + lyhyin)
        print("Pisin on " + pisin)
        print("Eka on " + lista[0])
        print("Vika on " + lista[-1])
        print("Keskimmäinen on " + lista[int(len(lista)/2)])
        break
    lista.append(sana)
    if len(sana) < len(lyhyin):
        lyhyin = sana
    if len(sana)> len(pisin):
        pisin = sana
