def kaikki_vaarinpain(lista):
    num0 = len(lista) - 1
    lista_uus = []
    while num0 >= 0:
        lista_uus.append(lista[num0][::-1])
        num0 -= 1
    return lista_uus
if __name__ == "__main__":
    lista = ["Moi", "kaikki", "esimerkki", "vielä yksi"]
    lista2 = kaikki_vaarinpain(lista)
    print(lista2)