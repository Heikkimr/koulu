def rivi_oikein(sudoku, rivi_nro):
    num0 = 0
    for number in sudoku[rivi_nro]:
        if number != 0 and num0 % number**2 == 0 and num0 % number**3 != 0:
            return(False)
        num0 += number**2
    return(True)