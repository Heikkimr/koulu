def positiivisten_summa(lista):
    num0 = 0
    num1 = 0
    while True:
        if lista[num0] > 0:
            num1 += lista[num0]
        num0 += 1
        if num0 == len(lista):
            return(num1)
if __name__ == "__main__":
    lista = [1, -2, 3, -4, 5]
    vastaus = positiivisten_summa(lista)
    print("vastaus", vastaus)