# 5) Tee ohjelma, joka kysyy käyttäjältä “Mitä kirjainta etsitään sukunimen 3:sta merkistä?”. Ohjelma tulostaa allekkain ne sukunimet, joiden 3:s kirjain on käyttäjän antama kirjain.
lista0 = ["Vanni", "Visanti", "Rantasalo", "Wuorimaa", "Kilpi", "Jalas", "Kaira", "Poijärvi", "Linnala", "Koskenniemi", "Arni", "Hainari",
"Pohjanpalo", "Jännes", "Kuusi", "Talas", "Rautapää", "Aura", "Wiherheimo", "Kuusisto", "Rantakari", "Pinomaa", "Paasilinna", "Pihkala",
"Halsti", "Kallia", "Haarla", "Harva", "Heikinheimo", "Päivänsalo", "Helanen", "Hattara", "Helismaa"]
check = input("Mitä kirjainta etsitään sukunimen 3:sta merkistä? ")
lista1 = [idx for idx in lista0 if idx[2].lower() == check.lower()]
num0 = 0
while True:
    if num0 == len(lista1):
        break
    print(lista1[num0])
    num0 += 1
