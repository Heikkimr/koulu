def joulukuusi(kertaa):
    print("joulukuusi!")
    num0 = 0
    num1 = 1
    num2 = kertaa*2-1
    while True:
        if kertaa <= num0:
            num2 = kertaa*2-1
            print(" "*int((num2-1)/2) + "*" + " "*int((num2-1)/2))
            break
        print(" "*int((num2/1)/2) + "*"*num1 + " "*int((num2-1)/2))
        num1 += 2
        num2 -= 2
        num0 +=1
if __name__ == "__main__":
    joulukuusi(5)
