import random
import os 
from os import system, name
from time import sleep
def clear():
    if name == 'nt':
        _ = system('cls')
    else:
        _ = system('clear')
# antaa pelaajalle kortin
def drawcardP():
    num0 = random.randint(1, 13)
    global player
    global playerI
    if num0 > 10:
        if num0 == 11:
            player += "J"
        elif num0 == 12:
            player += "Q"
        elif num0 == 13:
            player += "K"
    elif num0 == 1:
        player += "A"
    elif num0 == 10:
        player += "T"
    else:
        player.append(num0)   
    if num0 >= 11 and num0 <= 13:
        playerI.append(10)
    elif num0 == 1:
        playerI.append(11)
    else:
        playerI.append(num0)
# Antaa diilerille kortin
def drawcardD():
    num0 = random.randint(1, 13)
    global dealer
    global dealerI
    if num0 > 10:
        if num0 == 11:
            dealer += "J"
        elif num0 == 12:
            dealer += "Q"
        elif num0 == 13:
            dealer += "K"
    elif num0 == 1:
        dealer += "A"
    else:
        dealer.append(num0)   
    if num0 >= 11 and num0 <= 13:
        dealerI.append(10)
    elif num0 == 1:
        dealerI.append(11)
    else:
        dealerI.append(num0)
# tarkistaa, että onko joku mennyt yli 21
def bust(target):
    if target == dealer:
        target = dealerI
    if target == player:
        target = playerI
    if value(target) > 21:
        return(True)
    else:
        return(False)
# Palauttaa kohden korttien arvot
def value(target):
    num0 = 0
    num1 = 0
    ace = 0
    while num0 < len(target):
        if target[num0] == 11:
            ace += 1
        num0 += 1
    for number in target:
        num1 += number
    if num1 > 21 and ace > 0:
        num1 -= 10
        ace -= 1
    return(int(num1))
# Tarkistaa, onko jollakin blackjack
def blackjack(target):
    if len(target) == 2:
        if target[0] == 11 and target[1] == 10:
            return(True)
        elif target[1] == 11 and target[0] == 10:
            return(True)
# automaattisesti antaa diilerille sopivan määrän kortteja, jos pelaajalla on blackjack
def blackjackplayer():
    if blackjack(playerI) == True:
        if dealer[(len(dealer) - 1)] == "?":
            dealer.remove("?")
        while value(dealerI) < 17:
            drawcardD()
            printgame()
            sleep(1)
        if value(dealerI) == 21:
            return(False)
        else:
            return(True)
# Tarkistaa, että onko pelaaja voittanut tai hävinnyt True = pelaaja voitti False = diileri voitti "Draw" = tasapeli BustP = pelaaja meni yli 21 BustD = diileri meni yli 21 bjp = pelaajalla on blackjack bjd = diilerillä on blackjack no = kaikki ei ole tapahtunut
def win():
    if blackjackplayer() == True:
        return("bjp")
    elif blackjackplayer() == False:
        return("Draw")
    elif blackjack(dealerI) == True and value(playerI) != 21:
        return("bjd")
    elif bust(dealer) == True:
        return("BustD")
    elif value(playerI) and bust(player) == False and value(dealerI) < value(playerI) and value(dealerI) > 16: 
        return(True)
    elif bust(player) == True:
        return("BustP")
    elif value(playerI) < value(dealerI) and stand == True:
        return(False)
    elif value(playerI) == value(dealerI) and stand == True:
        return("Draw")
    else:
        return("no")

def printcards(input):
    global print1, print2, print3, print4, print5, print6
    def cardrow(input):
        global print1, print2, print3, print4, print5, print6
        print1 += input[0:6]
        print2 += input[7:14]
        print3 += input[14:21]
        print4 += input[21:28]
        print5 += input[28:35]
        print6 += input[35:42]
    num0 = 0
    carda = " _____ |A .  || /.\ ||(_._)||  |  ||____A|"
    card2 = " _____ |2    ||  ^  ||     ||  ^  ||___ 2|"
    card3 = " _____ |3    || ^ ^ ||     ||  ^  ||___ 3|"
    card4 = " _____ |4    || ^ ^ ||     || ^ ^ ||____4|"
    card5 = " _____ |5    || ^ ^ ||  ^  || ^ ^ ||____5|"
    card6 = " _____ |6    || ^ ^ || ^ ^ || ^ ^ ||____6|"
    card7 = " _____ |7    || ^ ^ ||^ ^ ^|| ^ ^ ||____7|"
    card8 = " _____ |8    ||^ ^ ^|| ^ ^ ||^ ^ ^||____8|"
    card9 = " _____ |9    ||^ ^ ^||^ ^ ^||^ ^ ^||____9|"
    card10 = " _____ |10^  ||^ ^ ^||^ ^ ^||^ ^ ^||___10|"
    cardj = " _____ |J  ww|| ^ {)||(.)%%|| | % ||__%%J|"
    cardq = " _____ |Q  ww|| ^ {(||(.)%%|| |%%%||_%%%Q|"
    cardk = " _____ |K  WW|| ^ {)||(.)%%|| |%%%||_%%%K|"
    cardque = " _____ |?    ||     ||     ||     ||____?|"
    while num0 < len(input):
        if input[num0] == "A":
            cardrow(carda)
        if input[num0] == "2":
            cardrow(card2)
        if input[num0] == "3":
            cardrow(card3)
        if input[num0] == "4":
            cardrow(card4)
        if input[num0] == "5":
            cardrow(card5)
        if input[num0] == "6":
            cardrow(card6)
        if input[num0] == "7":
            cardrow(card7)
        if input[num0] == "8":
            cardrow(card8)
        if input[num0] == "9":
            cardrow(card9)
        if input[num0] == "T":
            cardrow(card10)
        if input[num0] == "J":
            cardrow(cardj)
        if input[num0] == "Q":
            cardrow(cardq)
        if input[num0] == "K":
            cardrow(cardk)
        if input[num0] == "?":
            cardrow(cardque)
        print1 += "  "
        print2 += " "
        print3 += " "
        print4 += " "
        print5 += " "
        print6 += " "
        num0 += 1
    print(print1)
    print(print2)
    print(print3)
    print(print4)
    print(print5)
    print(print6)
    print1 = ""
    print2 = ""
    print3 = ""
    print4 = ""
    print5 = ""
    print6 = ""
# Tulostaa pelin
def printgame():
    temp = ""
    num0 = 0
    sleep(1)
    clear()
    print("-----------------------")
    print(f"Dealer {value(dealerI)}")
    while num0 < len(dealer):
        temp += str(dealer[num0])
        num0 += 1
    printcards(temp)
    print("-----------------------")
    print(f"Player {value(playerI)}")
    temp = ""
    num0 = 0
    while num0 < len(player):
        temp += str(player[num0])
        num0 += 1
    printcards(temp)
    print("-----------------------")
# printtaa tuloksen  
def printresult(result):
    clear()
    if result == True:
        print("-----------------------")
        print("You win with " + str(value(playerI)) + " points!")
        print("-----------------------")
    elif result == "bjp":
        print("-----------------------")
        print("You won with blackjack!")
        print("-----------------------")
    elif result == "bjd":
        print("-----------------------")
        print("Dealer won with blackjack!")
        print("-----------------------")
    elif result == False:
        print("-----------------------")
        print(f"Dealer won with " + str(value(dealerI)) +" points!")
        print("-----------------------")
    elif result == "Draw":
        print("-----------------------")
        print("You Drew!")
        print("-----------------------")
    elif result == "BustP":
        print("-----------------------")
        print("Player went bust!")
        print("-----------------------")
    elif result == "BustD":
        print("-----------------------")
        print("Dealer went bust!")
        print("-----------------------")
    else:
        print("The game has encountered an error.")
# Pelin logiikka
while True:
    player = []
    dealer = []
    playerI = []
    dealerI = []
    stand = False
    move = ""
    drawcardD()
    dealer += "?"
    drawcardP()
    drawcardP()
    printgame()
    if win() != "no":
        printresult(win())
    else:
        while True:
            hos = input("(S)tand or (H)it: ")
            hos = hos.lower()
            if hos == "h":
                drawcardP()
                printgame()
            if hos == "s":
                stand = True
                break
            if win() != "no":
                sleep(1)
                printresult(win())
                break
    if win() == "no":
        dealer.remove("?")
        while value(dealerI) < 17:
            drawcardD()
            printgame()
        printgame()
        if win() != "no":
            printresult(win())