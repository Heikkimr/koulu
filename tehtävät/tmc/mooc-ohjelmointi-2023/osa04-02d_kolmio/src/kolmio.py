# kopioi edellisestä tehtävästä funktion viiva koodi tänne
def viiva(kertaa, asia):
    print(asia[0:1]*kertaa)
    if asia == "":
        print("*"*kertaa)
def kolmio(koko):
    num0 = 0
    num1 = 1
    while True:
        if koko == num0:
            break
        num0 += 1
        viiva(num1, "#")
        num1 += 1
# funktiota kannattaa testata kutsumalla sitä täällä seuraavasti
if __name__ == "__main__":
    kolmio(5)
