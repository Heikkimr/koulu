# N) Tee funktio isoillaKirjaimilla, joka saa parametrinaan merkkijonon, joka on lause. Funktio palauttaa parametrina saadun lauseen kaikkien sanat isoilla alkukirjaimilla alkavina. Hyödynnä edellisen tehtävän funktiota, jossa muutetaan 1 sana.
def isoillaKirjaimilla():
    sana = input("Anna merkkijono: ")
    sana_uus = ""
    num0 = 0
    while True:
        if num0 == len(sana):
            print(sana_uus)
            break
        if num0 == 0:
            sana_uus += sana[num0].upper()
            num0 += 1
        elif sana[num0] == " ":
            sana_uus += sana[num0]
            num0 += 1
            sana_uus += sana[num0].upper()
            num0 += 1
        else:
            sana_uus += sana[num0]
            num0 += 1
# taas on salee parempi ratkaisu olemassa mutta tämä tuli mieleen ekana
isoillaKirjaimilla()
