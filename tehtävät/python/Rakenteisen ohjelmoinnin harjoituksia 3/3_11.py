# K) Tee funktio tahtia, joka saa parametrina merkkijonon. Funktio lisää merkkijonon jokaisen sanan väliin *-merkin ja merkkijonon loppuun !-merkin. Lisäksi funktio muuttaa saamansa merkkijonon merkit suuraakkosiksi.
def tahtia():
    sana = input("Anna merkkijono: ")
    sana = sana.replace(' ', "*")
    sana = sana.upper()
    print(sana + "!")
tahtia()
