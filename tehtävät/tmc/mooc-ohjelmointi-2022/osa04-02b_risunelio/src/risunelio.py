# kopioi edellisestä tehtävästä funktion viiva koodi tänne
def viiva(kertaa, asia):
    print(asia[0:1]*kertaa)
    if asia == "":
        print("*"*kertaa)

def risulaatikko(kertaa):
    num0 = 0
    while True:
        if kertaa == num0:
            break
        num0 +=1 
        viiva(10, "#")
def risunelio(koko):
    num1 = 0
    while True:
        if koko == num1:
            break
        num1 += 1
        viiva(koko, "#")

# funktiota kannattaa testata kutsumalla sitä täällä seuraavasti
if __name__ == "__main__":
    risunelio(5)
