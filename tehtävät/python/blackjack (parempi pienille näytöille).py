import random
# Miksi in player ja playerI?
# playerI on vain siihen käyttöön että se voi tarkistaa pisteet helpommin, ennen se koodi oli niin buginen että peli ei toiminut kunolla
# antaa pelaajalle kortin
def drawcardP():
    num0 = random.randint(1, 13)
    global player
    global playerI
    if num0 > 10:
        if num0 == 11:
            player += "J"
        elif num0 == 12:
            player += "Q"
        elif num0 == 13:
            player += "K"
    elif num0 == 1:
        player += "A"
    else:
        player.append(num0)   
    if num0 >= 11 and num0 <= 13:
        playerI.append(10)
    elif num0 == 1:
        playerI.append(11)
    else:
        playerI.append(num0)
# Antaa diilerille kortin
def drawcardD():
    num0 = random.randint(1, 13)
    global dealer
    global dealerI
    if num0 > 10:
        if num0 == 11:
            dealer += "J"
        elif num0 == 12:
            dealer += "Q"
        elif num0 == 13:
            dealer += "K"
    elif num0 == 1:
        dealer += "A"
    else:
        dealer.append(num0)   
    if num0 >= 11 and num0 <= 13:
        dealerI.append(10)
    elif num0 == 1:
        dealerI.append(11)
    else:
        dealerI.append(num0)
# tarkistaa, että onko joku mennyt yli 21
def bust(target):
    if target == dealer:
        target = dealerI
    if target == player:
        target = playerI
    if value(target) > 21:
        return(True)
    else:
        return(False)
# Palauttaa kohden korttien arvot
def value(target):
    num0 = 0
    num1 = 0
    ace = 0
    while num0 < len(target):
        if target[num0] == 11:
            ace += 1
        num0 += 1
    for number in target:
        num1 += number
    if num1 > 21 and ace > 0:
        num1 -= 10
        ace -= 1
    return(num1)
# Tarkistaa, että onko pelaaja voittanut tai hävinnyt True = pelaaja voitti False = diileri voitti "Draw" = tasapeli BustP = pelaaja meni yli 21 BustD = diileri meni yli 21 no = kaikki ei ole tapahtunut
def win():
    if bust(dealer) == True:
        return("BustD")
    elif value(playerI) and bust(player) == False and value(dealerI) < value(playerI) and value(dealerI) > 16: 
        return(True)
    elif bust(player) == True:
        return("BustP")
    elif value(playerI) < value(dealerI) and stand == True:
        return(False)
    elif value(playerI) == value(dealerI) and stand == True:
        return("Draw")
    else:
        return("no")
# Tulostaa pelin
def printgame():
    print("------------------")
    print(f"Dealer {dealer} {value(dealerI)}")
    print(f"Player {player} {value(playerI)}")
    print("------------------")
# printtaa tuloksen  
def printresult(result):
    if result == True:
        print("You win with " + str(value(playerI)) + " points!")
    elif result == False:
        print(f"Dealer won with " + str(value(dealerI)) +" points!")
    elif result == "Draw":
        print("You Drew!")
    elif result == "BustP":
        print("Player went bust!")
    elif result == "BustD":
        print("Dealer went bust!")
    else:
        print("The game has encountered an error.")
# Pelin logiikka
player = []
dealer = []
playerI = []
dealerI = []
stand = False
move = ""
drawcardD()
drawcardP()
drawcardP()
printgame()
if win() == True or win() == False or win() == "Draw" or win() == "BustP" or win() == "BustD":
    printresult(win())
while True:
    hos = input("(S)tand or (H)it: ")
    if hos == "h":
        drawcardP()
        printgame()
    if hos == "s":
        stand = True
        break
    if win() == True or win() == False or win() == "Draw" or win() == "BustP" or win() == "BustD":
        printresult(win())
        break
if win() == "no":
    while value(dealerI) < 17:
        drawcardD()
    printgame()
    if win() == True or win() == False or win() == "Draw" or win() == "BustP" or win() == "BustD":
        printresult(win())