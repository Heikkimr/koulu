# 9) Tee ohjelma, joka pyytää käyttäjältä kokonaisluvun ja kertoo onko luku 60 tai sitä pienempi tai ei kumpaakaan:
num0 = int(input("Kerro luku: "))
if num0 == 60:
    print("Luku on tasan 60")
elif num0 > 60:
    print("Luku ei ole 60 eikä ole pienempi")
else:
    print("Luku on pienempi kuin 60")
