# 14) Tee ohjelma, joka pyytää käyttäjältä kokonaislukuja ja laskee lukujen summan. Nolla lopettaa.
num0 = 0
num1 = 0
while True:
    num0 = int(input("Kerro numero, nolla lopettaa: "))
    num1 += num0
    if num0 == 0:
        print("Lukujen summa on " + str(num1))
        break
