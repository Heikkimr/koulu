# B) Tee ohjelma, joka kysyy käyttäjältä sanan. Sitten ohjelma muuttaa sanassa kaikki a ja A -kirjaimet b tai B -kirjaimiksi ja tulostaa näin muodostetun sanan. Älä käytä Pythonin stringin replace()-metodia.
sana = input("Kerro sana: ")
sana_uus = ""
sana_v = ""
num0 = 0
while True:
    if num0 == len(sana):
        break
    sana_v = sana[num0]
    if sana_v == "A":
        sana_v = "B"
    if sana_v == "a":
        sana_v = "b"
    sana_uus += sana_v
    sana_v = ""
    num0 += 1
print(sana_uus)
