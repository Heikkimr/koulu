# C) Tee funktio, joka muuntaa annetun sanan kaikki e ja E-kirjaimet f tai F -kirjaimiksi ja palauttaa näin muodostetun sanan. Käytä Pythonin stringin replace()-metodia.
sana = input("Kerro sana: ")
sana_uus = ""
sana_v = ""
num0 = 0
while True:
    if num0 == len(sana):
        break
    sana_v = sana[num0]
    if sana_v == "e":
        sana_v = "f"
    elif sana_v == "E":
        sana_v = "f"
    sana_uus += sana_v
    sana_v = ""
    num0 += 1
print(sana_uus)
