def vaihteluvali(lista):
    num0 = 0
    num1 = 100000
    num2 = 0
    while num0 < len(lista):
        if lista[num0] < num1:
            num1 = lista[num0]
        if lista[num0] > num2:
            num2 = lista[num0]
        num0 += 1
    return(num2 - num1)
if __name__ == "__main__":
    lista = [1, 2, 3, 4, 5]
    vaihteluvali([1, 2, 3])
