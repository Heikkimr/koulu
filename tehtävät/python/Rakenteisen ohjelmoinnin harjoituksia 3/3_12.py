# L) Tee funktio jokaToinenMerkki, joka saa parametrina merkkijonon. Funktio muuttaa parametrina saadun merkkijonon joka toisen merkin suuraakkosiksi ja palauttaa muutetun merkkijonon.
def jokaToinenMerkki():
    sana = input("Anna merkkijono: ")
    sana_uus = ""
    num0 = 0
    while True:
        if num0 >= len(sana):
            break
        sana_uus += sana[num0]
        num0 += 1
        if num0 >= len(sana):
            break
        sana_uus += sana[num0].upper()
        num0 += 1
    print(sana_uus)
jokaToinenMerkki()
