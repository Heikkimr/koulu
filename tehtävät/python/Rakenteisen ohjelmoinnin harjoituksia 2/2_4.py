# 4) Tee ohjelma, joka kopioi listasta uuteen listaan ne sukunimet, jotka päättyvät s-kirjaimeen. Sen jälkeen ohjelma tulostaa uuden listan alkiot aakkosjärjestyksessä allekkain.
lista0 = ["Vanni", "Visanti", "Rantasalo", "Wuorimaa", "Kilpi", "Jalas", "Kaira", "Poijärvi", "Linnala", "Koskenniemi", "Arni", "Hainari",
"Pohjanpalo", "Jännes", "Kuusi", "Talas", "Rautapää", "Aura", "Wiherheimo", "Kuusisto", "Rantakari", "Pinomaa", "Paasilinna", "Pihkala",
"Halsti", "Kallia", "Haarla", "Harva", "Heikinheimo", "Päivänsalo", "Helanen", "Hattara", "Helismaa"]
check = "s"
lista1 = [idx for idx in lista0 if idx[-1].lower() == check.lower()]
print(lista1[0])
print(lista1[1])
print(lista1[2])
