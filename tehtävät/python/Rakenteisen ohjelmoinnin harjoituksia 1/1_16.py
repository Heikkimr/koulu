# 16) Kirjoita ohjelmaan lista, jossa on sukunimiä: ["Pekkala","Suominen","Pitkämäki","Männikkö","Tapanila","Mäki"]
lista0 = ["Pekkala", "Suominen", "Pitkämäki", "Männikkö", "Tapanila", "Mäki"]
in0 = input("Millä alkukirjaimella etsitään?")
if in0 == "P":
    print(lista0[0])
    print(lista0[2])
elif in0 == "S":
    print(lista0[1])
elif in0 == "M":
    print(lista0[3])
    print(lista0[5])
elif in0 == "T":
    print(lista0[4])
else:
    print("Listassa ei ole sillä alkukirjaimella alkavaa nimeä.")
