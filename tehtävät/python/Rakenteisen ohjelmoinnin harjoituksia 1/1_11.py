# 11) Tee ohjelma, joka kysyy paljonko sinulla on rahaa ja paljonko pitsa maksaa:
raha0 = float(input("Paljonko rahaa sinulla on?"))
raha1 = float(input("Paljonko maksaa pitsa?"))
if raha0 >= raha1:
    print("Nauti pitsasta!")
    print("Rahaa jäi " + str(raha0 - raha1))
else:
    print("Rahasi eivät riitä pitsaan.")
