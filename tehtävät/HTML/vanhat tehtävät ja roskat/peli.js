console.log("Ha haa!");
//määritellään muuttuja, joka pysyy samana
//hae html koodista kanvaasi
const canvas = document.getElementById('canvas');
//2d
const ctx = canvas.getContext('2d');
//let on muuttuja, joka vaihtelee
let raf;
let inputStates = {};
window.addEventListener('keydown', function(event) {
if (event.key =="d") {
  inputStates.right = true;
}
if (event.key =="a") {
  inputStates.left = true;
}
}, false);
window.addEventListener('keyup', function(event) {
  if (this.event.key =="d") {
    inputStates.right = false;
  }
  if (this.event.key =="a") {
    inputStates.left = false; 
  }
}
);

//javascriptin objektin määrittely, ominaisuudet voivat muuttaa
const pisteet = {
  määrä: 0
}
const ball = {
  x: 100,
  y: 100,
  vx: 6,  //nopeus ja suunta
  vy: 2,  
  radius: 25, 
  color: 'blue',
  draw() {
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
    ctx.closePath();
    ctx.fillStyle = this.color;
    ctx.fill();
  }
};

const maila = {
  x: 250,
  y: 278,
  vx: 5,  //nopeus ja suunta
  width : 100,
  height : 30,
  color : 'red',
  draw() {
  ctx.fillstyle = this.color;
  ctx.fillRect(this.x, this.y, this.width, this.height);
  }
 };
 
function draw() {
  document.getElementById("pis.").innerHTML = pisteet.määrä;
  //tyhjennä
  ctx.clearRect(0,0, canvas.width, canvas.height);
  //piirrä pelimaailma
  ball.draw();
  maila.draw();
//liikuta pelimaailma, kaikki liikkuvat
  ball.x += ball.vx;
  ball.y += ball.vy;
// liikuta mailaa
if (inputStates.right) {
  maila.x = maila.x + maila.vx;
}
if (inputStates.left) {
  maila.x = maila.x - maila.vx;
}


  if (ball.y + ball.vy > canvas.height - 25||
      ball.y + ball.vy < 25) {
    ball.vy = -ball.vy;
  }
  if (ball.x + ball.vx > canvas.width - 25||
      ball.x + ball.vx < 25) {
    ball.vx = -ball.vx;
  }  
  
  if(maila.x + 50> canvas.width) {
    maila.x = maila.x - 51;
  }
  if(maila.x < 0) {
    maila.x = maila.x + 50;
  }
var testX = ball.x;
var testY = ball.y;

if(testX < maila.x) testX = maila.x;
else if (testX > (maila.x + maila.width)) testX = (maila.x + maila.width);
if(testY < maila.y) testY = maila.y
else if(testY > (maila.y + maila.height)) testY = (maila.y + maila.height);

var distX = ball.x - testX;
var distY = ball.y - testY;
var dista = Math.sqrt((distX*distX) +  (distY*distY))

if(dista <= ball.radius) {
  if(ball.x >= maila.x && ball.x <= (maila.x + maila.width)) {
    ball.vx *=-1;
    pisteet.määrä = pisteet.määrä + (1);
  }
} else if(ball.y >= maila.y && ball.y <= (maila.y + maila.height)) {
  ball.vx *= -1;
  pisteet.määrä = pisteet.määrä + (1);
}
raf = window.requestAnimationFrame(draw);
}
draw();



// lähde https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Advanced_animations