#D) Tee ohjelma, joka kyselee käyttäjältä kokonaislukuja 1 - 9 väliltä ja tallentaa ne listaan. Nolla lopettaa käyttäjän syötteen, eikä sitä tule tallentaa listaan.
# Sitten ohjelma lisää listan alkion arvoon ko. alkion indeksin listassa. Sitten ohjelma tulostaa listan.
num0 = 0
num1 = 0
lista = []
while True:
    num0 = int(input("Kerro numero 1-9: "))
    if num0 == 0:
        print(lista)
        break
    lista.append(num0 + num1)
    num1 += 1
