# M) Tee funktio isoAlkukirjain, joka saa parametrina merkkijonon, joka on 1 sana. Funktio palauttaa parametrina saadun sanan isolla alkukirjaimella alkavana.
def isollaAlkukirjaimella():
    sana = input("Kerro sana: ")
    sana_iso = sana[0].upper() + sana[1:(len(sana))]
    print(sana_iso)
isollaAlkukirjaimella()
