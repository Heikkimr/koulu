def ilman_vokaaleja(string):
    num0 = 0
    vokaalit = ["a", "e", "u", "o", "y", "i", "ö", "ä", "å"]
    while num0 < len(vokaalit):
        string = string.replace(vokaalit[num0], "") 
        num0 += 1
    return string
if __name__ == "__main__":
    mjono = "tämä on esimerkki"
    print(ilman_vokaaleja(mjono))