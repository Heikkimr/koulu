def kumpi_voitti(a):
    num0 = 0
    num1 = 0
    for number in a:
        for number0 in number:
            if number0 == 1:
                num0 += 1
            elif number0 == 2:
                num1 += 1
    if num0 > num1:
        return(1)
    elif num0 < num1:
        return(2)
    else:
        return(0)