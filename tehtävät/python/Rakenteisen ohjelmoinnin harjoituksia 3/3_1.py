# A) Tee funktio, joka muuntaa annetun DNA:n RNA:ksi. Älä käytä Pythonin stringin replace()-metodia.
dna = "GGCATTCAG"
rna = ""
dna_rna = ""
num0 = 0
while True:
    if num0 == len(dna):
        break
    dna_rna = dna[num0]
    if dna_rna == "T":
        dna_rna = "U"
    rna += dna_rna
    dna_rna = ""
    num0 += 1
print(rna)
