# P) Tee funktio pituusjarjestykseen, joka saa parametrinaan listan, jonka alkiot ovat merkkijonoja. Funktio järjestää listan uudelleen merkkijonon pituuden mukaiseen järjestykseen, lyhyin merkkijono ensin, ja palauttaa järjestetyn listan.
def pituusjarjestykseen():
    lista = ["",]
    num0 = 0
    num1 = 0
    sana = input("Kerro merkkijono, tyhjä lopettaa: ")
    lista.append(sana)
    num0 += 1
    while True:
        sana = input("Kerro merkkijono, tyhjä lopettaa: ")
        num1 = len(lista)
        if sana == "":
            print(lista)
            break
        while True:
            if len(sana) < len(lista[num1-1]):
                num1 -= 1
            else:
                lista.insert(num1, sana)
                print(num1)
                break
pituusjarjestykseen()
# 